var baseArray = ['A', 'B', 'C', 1];

function buildArray(repVal) {
    return new Array(3).fill(repVal).flat();
}

console.log("This is the resulting array: ", buildArray(baseArray));