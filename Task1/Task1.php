<?php
$baseArray = ['A','B','C', 1];

function buildArray($ary) {
    $newArry = [];

    for ($x = 0; $x < 3; $x++) {
        foreach ($ary as &$value) {
            array_push($newArry, $value);
        }
    }
    return $newArry;
}

//$repeat = buildArray($baseArray);
print_r(buildArray($baseArray));