const string = "liMeSHArp DeveLoper TEST"

function vowelRemove(string){
  
    let characterArray = string.split("")

    let vowelStripped = characterArray.map(character => {
        if(/[aeiouyAEIOUY]/.test(character)){
            character = ""
        } else {return character}
    }).join("").toLowerCase()

    vowelStripped = vowelStripped.charAt(0).toUpperCase() + vowelStripped.slice(1);
    
    return vowelStripped
}

console.log(vowelRemove(string))