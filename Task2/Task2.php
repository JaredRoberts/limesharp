<?php
$string = "liMeSHArp DeveLoper TEST";

function stripString ($str) {
    $vowels = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U");
    $strippedVowels = str_replace($vowels, "", $str);
    $lowecaseStripped = strtolower($strippedVowels);
    $finalStripped = ucfirst($lowecaseStripped);
    return $finalStripped;
}

print_r(stripString($string));